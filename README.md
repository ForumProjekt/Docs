# Pointless Documentation

Powered by **Daux.io** documentation generator that uses a simple folder structure and Markdown files to create custom documentation on the fly.

## How to für Windows User

1) Putty herunterladen => [x64](https://the.earth.li/~sgtatham/putty/latest/w64/putty.exe) oder [x86](https://the.earth.li/~sgtatham/putty/latest/w32/putty.exe)

2) Heruntergeladene EXE mit [Checksumme](https://the.earth.li/~sgtatham/putty/0.70/sha256sums) überprüfen => **Ich will keine scheiße auf meinem Server:**
* CMD Öffnen und in Downloads Ordner Navigieren  
* `CertUtil -hashfile <Putty Dateiname>.exe SHA256` ausführen und Hash mit Checksummen Datei vergleichen (`w64/putty.exe` oder `w32/putty.exe`)  
* Alternativ wenn 7-ZIP Installiert ist im Explorer Kontextmenü von Putty.exe `CRC SHA` aufklappen und SHA-256 auswählen  

3) Dieses repo clonen und in Ordner `/docs/` wechseln

4) Doku bearbeiten, hinzufügen => [Abschnitte unten](#dateien-für-doku)

5) **Upload auf `docs.pointless.ga`**
- Änderungen normal Commiten
- Putty einrichten:
	* Putty öffnen und im Feld `Host Name (or IP address)` `host1.unbekannt3.eu` eintragen (Falls ISP Kabel Deutschland ist und DNS nicht geht bitte IPv6 Adresse nehmen: `2a01:7e0:0:417:c9::9a3c`)
	* Im Feld `Saved Sessions` kann ein Name vergeben werden und dann auf `Save` geklickt werden, um nicht jedes mal wieder den Hostnamen eingeben zu müssen
	* Doppelt auf gespeicherten Eintrag klicken um Session zu öffnen
- Login:
	* Wenn verbindung Hergestellt: 
		* `login as:` => Benutzername => `projektDocs`
		* `projektDocs@host1.unbekannt3.eu's password:` => Passwort wie für PHPMyAdmin Datenbank nehmen (Die eingabe wird nicht angezeigt!)
- Benötigtes Programm zum PATH in Linux Session hinzufügen:
	* `export PATH=$PATH:~/.composer/vendor/bin` => Mit Rechtsklick kann in Putty eingefügt werden
- In richtigen Ordner wechseln: `cd .composer/vendor/daux/daux.io/`
- Server repo aktualisieren: `git pull`
- Markdown Dateien in für Browser lesbare HTML Dateien Kompilieren: `daux generate`
- **Änderungen sollten live sein => Webserver liest kompilierte Dateien aus `/static/`**

## Dateien für Doku

Der Generator erkennt alle Markdown Dateien die mit `.md` und  `.markdown` enden und innerhalb des `/docs/` Verzeichnisses liegen.

Jeder Unterordner bildet eine eigene Kategorie ab, welche später im Menü durch klicken geöffnet wird.

In Datei- und Ordnernamen darf kein `SPACE` vorkommen, ersetzte dies einfach mit einem UNDERSCORE (`_`), diese werden am ende zu Leertasten konvertiert:

**So geht's:**

* 01_Getting_Started.md = Getting Started
* API_Calls.md = API Calls
* 200_Something_Else-Cool.md = Something Else-Cool
* _5_Ways_to_Be_Happy.md = 5 Ways To Be Happy

**So nicht:**

* File Name With Space.md = FAIL

## Reihenfolge im Menü

Um die Reihenfolge der Anzeige zu ändern hänge an jede Datei/Ordner einfach vor den eigentlichen Dateinamen eine Nummer im Format `00_`, z.B. `/docs/01_Hello_World.md` und `/docs/05_Features.md`.  
Dadurch wird erreicht, dass *Hello World* immer vor *Features* angezeigt.  
Um in einen Dateinamen am anfang eine Zahl benutzen zu können **muss** vor dieser Zahl ein UNDERSCORE benutzt werden, da diese sonst herausgefiltert wird (z.B. `/docs/_6_Ways_to_Get_Rich.md`).

## Kategorie Startseite

Wenn für eine einzelne Kategorie eine Übersichtsseite benötigt wird kann diese durch einfaches erstellen einer `_index.md` Datei innerhalb des Kategorieordners erstellt werden.
